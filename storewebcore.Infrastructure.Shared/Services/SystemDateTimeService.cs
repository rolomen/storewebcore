﻿using storewebcore.Application.Interfaces.Shared;
using System;

namespace storewebcore.Infrastructure.Shared.Services
{
    public class SystemDateTimeService : IDateTimeService
    {
        public DateTime NowUtc => DateTime.UtcNow;
    }
}