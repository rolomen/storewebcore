﻿using storewebcore.Application.DTOs.Mail;
using System.Threading.Tasks;

namespace storewebcore.Application.Interfaces.Shared
{
    public interface IMailService
    {
        Task SendAsync(MailRequest request);
    }
}