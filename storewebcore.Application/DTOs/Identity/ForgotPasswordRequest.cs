﻿using System.ComponentModel.DataAnnotations;

namespace storewebcore.Application.DTOs.Identity
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}