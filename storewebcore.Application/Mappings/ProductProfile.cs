﻿using storewebcore.Application.Features.Products.Commands.Create;
using storewebcore.Application.Features.Products.Queries.GetAllCached;
using storewebcore.Application.Features.Products.Queries.GetAllPaged;
using storewebcore.Application.Features.Products.Queries.GetById;
using storewebcore.Domain.Entities.Catalog;
using AutoMapper;

namespace storewebcore.Application.Mappings
{
    internal class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<CreateProductCommand, Product>().ReverseMap();
            CreateMap<GetProductByIdResponse, Product>().ReverseMap();
            CreateMap<GetAllProductsCachedResponse, Product>().ReverseMap();
            CreateMap<GetAllProductsResponse, Product>().ReverseMap();
        }
    }
}