﻿using storewebcore.Application.Features.Brands.Commands.Create;
using storewebcore.Application.Features.Brands.Queries.GetAllCached;
using storewebcore.Application.Features.Brands.Queries.GetById;
using storewebcore.Domain.Entities.Catalog;
using AutoMapper;

namespace storewebcore.Application.Mappings
{
    internal class BrandProfile : Profile
    {
        public BrandProfile()
        {
            CreateMap<CreateBrandCommand, Brand>().ReverseMap();
            CreateMap<GetBrandByIdResponse, Brand>().ReverseMap();
            CreateMap<GetAllBrandsCachedResponse, Brand>().ReverseMap();
        }
    }
}