﻿using storewebcore.Infrastructure.Identity.Models;
using storewebcore.Web.Areas.Admin.Models;
using AutoMapper;

namespace storewebcore.Web.Areas.Admin.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<ApplicationUser, UserViewModel>().ReverseMap();
        }
    }
}