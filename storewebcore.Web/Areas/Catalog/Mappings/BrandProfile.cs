﻿using storewebcore.Application.Features.Brands.Commands.Create;
using storewebcore.Application.Features.Brands.Commands.Update;
using storewebcore.Application.Features.Brands.Queries.GetAllCached;
using storewebcore.Application.Features.Brands.Queries.GetById;
using storewebcore.Web.Areas.Catalog.Models;
using AutoMapper;

namespace storewebcore.Web.Areas.Catalog.Mappings
{
    internal class BrandProfile : Profile
    {
        public BrandProfile()
        {
            CreateMap<GetAllBrandsCachedResponse, BrandViewModel>().ReverseMap();
            CreateMap<GetBrandByIdResponse, BrandViewModel>().ReverseMap();
            CreateMap<CreateBrandCommand, BrandViewModel>().ReverseMap();
            CreateMap<UpdateBrandCommand, BrandViewModel>().ReverseMap();
        }
    }
}