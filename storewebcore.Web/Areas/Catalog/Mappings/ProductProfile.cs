﻿using storewebcore.Application.Features.Products.Commands.Create;
using storewebcore.Application.Features.Products.Commands.Update;
using storewebcore.Application.Features.Products.Queries.GetAllCached;
using storewebcore.Application.Features.Products.Queries.GetById;
using storewebcore.Web.Areas.Catalog.Models;
using AutoMapper;

namespace storewebcore.Web.Areas.Catalog.Mappings
{
    internal class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<GetAllProductsCachedResponse, ProductViewModel>().ReverseMap();
            CreateMap<GetProductByIdResponse, ProductViewModel>().ReverseMap();
            CreateMap<CreateProductCommand, ProductViewModel>().ReverseMap();
            CreateMap<UpdateProductCommand, ProductViewModel>().ReverseMap();
        }
    }
}