﻿using Microsoft.AspNetCore.Mvc;

namespace storewebcore.Web.Views.Shared.Components.Logout
{
    public class LogoutViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}