﻿using Microsoft.AspNetCore.Mvc;

namespace storewebcore.Web.Views.Shared.Components.FormModal
{
    public class FormModalViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}